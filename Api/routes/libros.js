'use strict';

var Libro = require('../models/Libro');
var express = require('express');
var router = express.Router();

/* GET libros listing. */
router.get('/', async function (req, res) {
    const libros = await Libro.findAll();
    res.json(libros);
});

/* POST libros. */
router.post('/', async function (req, res) {

    let { Titulo, Descripcion, Autor, Editorial } = req.body;

    let libro = await Libro.create({
        titulo: Titulo,
        descripcion: Descripcion,
        autor: Autor,
        editorial: Editorial
    });

    res.json(libro);
});

/* PUT libros. */
router.put('/:id', async function (req, res) {

    const id = req.params.id;

    Libro.update({
        titulo: req.body.Titulo,
        descripcion: req.body.Descripcion,
        autor: req.body.Autor,
        editorial: req.body.Editorial,
    }, {
        where: { id: req.params.id }
    }).then(result => {
        res.send({
            message: "Libro actualizado"
        });
        res.status(200).json(result);
    });
    res.json(libros);
});


/* DELETE libros. */
router.delete('/:id', async function (req, res) {
    const id = req.params.id;
    Libro.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "El libro fue eliminado"
                });
            } else {
                res.send({
                    message: "No se pudo eliminar el libro con el id=" + id
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "No se pudo eliminar el libro con el id=" + id
            });
        });
    res.json(libros);
});

module.exports = router;
